package io.example.timer.ui.components

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import io.example.timer.ui.theme.TimerTheme
import io.example.timer.viewmodels.TimerViewModel

@ExperimentalAnimationApi
@Composable
fun TimerComponent(viewModel: TimerViewModel) {
    TimerTheme {
        Box(contentAlignment = Alignment.Center, modifier = Modifier.clickable {
            viewModel.timerClicked()
        }) {
            Text(
                viewModel.sliderValue.value.toInt().toString(),
                color = Color.Black,
                fontSize = 35.sp
            )
            Canvas(
                modifier = Modifier
                    .size(250.dp)
                    .padding(16.dp)
            ) {
                drawCircle(
                    SolidColor(Color.LightGray),
                    size.width / 2,
                    style = Stroke(45f)
                )
                drawArc(
                    brush = SolidColor(Color.Black),
                    startAngle = -90f,
                    sweepAngle = viewModel.convertedValue.value,
                    useCenter = false,
                    style = Stroke(45f)
                )
            }
        }
    }
}

@ExperimentalAnimationApi
@Preview(showBackground = true)
@Composable
fun TimerComponentPreview() {
    TimerComponent(TimerViewModel())
}