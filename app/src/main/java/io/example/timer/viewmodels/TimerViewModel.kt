package io.example.timer.viewmodels

import android.os.CountDownTimer
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel

class TimerViewModel : ViewModel() {
    companion object {
        const val DURATION = 5000F
        const val INTERVAL = 10L
        const val MAX_DEGREES = 360F
        const val ONE_SECOND = 1000F
        const val BACK_DURATION = 300F
    }

    var timerIsRunning = false
    var timerCompleted = false
    var lastMillisTimer = 0L
    val convertedValue = mutableStateOf(0f)
    val sliderValue = mutableStateOf(0f)
    lateinit var timer: CountDownTimer

    init {
        initTimer()
    }

    private fun initTimer() {
        timer = object : CountDownTimer(
            if (lastMillisTimer == 0L) DURATION.toLong() else lastMillisTimer,
            INTERVAL
        ) {
            override fun onTick(millisUntilFinished: Long) {
                onTimerTick(millisUntilFinished)
            }

            override fun onFinish() {
                onTimerFinish()
            }
        }
    }

    fun startTimer() {
        timer.start()
    }

    fun stopTimer() {
        timerIsRunning = false
        timer.cancel()
    }

    private fun onTimerTick(millisUntilFinished: Long) {
        timerIsRunning = true
        timerCompleted = false
        lastMillisTimer = millisUntilFinished
        sliderValue.value = (DURATION - lastMillisTimer) / ONE_SECOND
        val time = (lastMillisTimer - DURATION) / DURATION
        val total = (time * -1) * MAX_DEGREES
        convertedValue.value = total
    }

    private fun onTimerFinish() {
        timerIsRunning = false
        sliderValue.value = DURATION / ONE_SECOND
        convertedValue.value = MAX_DEGREES
        lastMillisTimer = 0
        stopTimer()
        timerCompleted = true
    }

    fun timerClicked() {
        if (timerCompleted) {
            resetTimer()
        } else {
            playOrPauseTimer()
        }
    }

    private fun resetTimer() {
        timer.cancel()
        timer = object : CountDownTimer(
            BACK_DURATION.toLong(),
            INTERVAL
        ) {
            override fun onTick(millisUntilFinished: Long) {
                val time = millisUntilFinished / BACK_DURATION
                val total = time * MAX_DEGREES
                convertedValue.value = total
            }

            override fun onFinish() {
                timerIsRunning = false
                sliderValue.value = 0f
                convertedValue.value = 0f
                lastMillisTimer = 0
                stopTimer()
                timerCompleted = false
            }
        }
        startTimer()
    }


    private fun playOrPauseTimer() {
        if (timerIsRunning) {
            stopTimer()
        } else {
            timer.cancel()
            initTimer()
            startTimer()
        }
    }
}